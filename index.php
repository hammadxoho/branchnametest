<?php
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=file.csv");
header("Pragma: no-cache");
header("Expires: 0");

$file = fopen("tbl_catalyst_rundown_data.csv", "r");

$wordArray = array();

while (!feof($file)) {
    $myCol = fgetcsv($file);

    $stringToParse = str_replace(array('(', ')', ',', '.'), '', $myCol[0]);

    $explodedString = explode(' ', $stringToParse);

    foreach ($explodedString as $singleValue) {
        if (trim($singleValue)) {
            
            $singleValue = strtolower($singleValue);
            if(strlen($singleValue) >=3){
                if (key_exists($singleValue, $wordArray)) {
                    $wordArray[$singleValue] ++;
                } else {
                    $wordArray[$singleValue] = 1;
                }
            }
        }
    }
}
arsort($wordArray);
foreach($wordArray as $keyWord => $singleItem){
    echo $keyWord .", ".$singleItem ." times \n";
}

fclose($file);
exit;
?>